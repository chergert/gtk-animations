all: canvas

OBJS = canvas.o ppg-animation.o animator.o uber-frame-source.o uber-timeout-interval.o ppg-header.o ppg-ruler.o

%.o: %.c %.h
	$(CC) -g -c -o $@ -Wall -Werror $(shell pkg-config --libs --cflags goocanvas) $*.c

canvas.o: canvas.c
	$(CC) -g -c -o $@ -Wall $(shell pkg-config --libs --cflags goocanvas) canvas.c

canvas: $(OBJS)
	$(CC) -g -o $@ -Wall -Werror $(shell pkg-config --libs --cflags goocanvas) $(OBJS)

clean:
	rm -f canvas *.o
