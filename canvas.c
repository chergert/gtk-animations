#include <goocanvas.h>
#include <gtk/gtk.h>

#include "animator.h"
#include "ppg-ruler.h"

static GtkWindow        *window;
static GtkAdjustment    *vadj;
static GList            *rows;
static PpgAnimationMode  mode = PPG_ANIMATION_EASE_IN_OUT_QUAD;

static void
reset_resizable (gpointer data)
{
	g_object_set(window,
	             "width-request", -1,
	             "height-request", -1,
	             "resizable", TRUE,
	             NULL);
	g_timeout_add(1000, (GSourceFunc)gtk_main_quit, NULL);
}

static void
shrink (gpointer data)
{
	g_object_animate_full(window, mode, 1000, 30,
	                      (GDestroyNotify)reset_resizable, NULL,
	                      "width-request", 640,
	                      "height-request", 480,
	                      "opacity", 1.0,
	                      NULL);
}

static void
hide (gpointer data)
{
	gint position;

	g_object_get(data, "position", &position, NULL);
	g_object_animate(data, mode, 1000,
	                 "position", position + 300,
	                 NULL);

	g_object_set(window,
	             "width-request", 640,
	             "height-request", 480,
	             "resizable", FALSE,
	             NULL);
	g_object_animate_full(window, mode, 1000, 30,
	                      (GDestroyNotify)shrink, NULL,
	                      "width-request", 800,
	                      "height-request", 600,
	                      "opacity", 0.5,
	                      NULL);
}

static void
scroll (gpointer data)
{
	g_object_animate_full(vadj, mode, 1000, 0,
	                      (GDestroyNotify)hide, data,
	                      "value", 200.0,
	                      NULL);
}

static gboolean
animate (gpointer data)
{
	gint position;

	g_object_get(data, "position", &position, NULL);
	g_object_animate_full(data, mode, 1000, 0,
	                      (GDestroyNotify)scroll, data,
	                      "position", position - 300,
	                      NULL);
	return FALSE;
}

static void
size_allocate (GtkWidget *widget,
               GtkAllocation *alloc,
               GooCanvasItem *table)
{
	GList *iter;

	g_object_set(table,
	             "width", (gdouble)alloc->width - 1.0,
	             NULL);

	for (iter = rows; iter; iter = iter->next) {
		g_object_set(iter->data,
		             "width", (gdouble)alloc->width - 201.0f,
		             NULL);
	}
}

static void
ruler_size_allocate (GtkWidget *ruler,
                     GtkAllocation *alloc)
{
	g_object_set(ruler,
	             "upper", (gdouble)alloc->width / 40.0,
	             NULL);
}

static void
notify_height (GooCanvasItem *table,
               GParamSpec *pspec)
{
	gdouble height;
	g_object_get(table, "height", &height, NULL);
	g_debug("Height is now %f", height);
}

gint
main (gint   argc,
      gchar *argv[])
{
	GtkWidget *scroller;
	GtkWidget *l;
	GtkWidget *paned;
	GtkWidget *vbox;
	GtkWidget *ruler;
	GtkWidget *layout;
	GtkWidget *h;
	GooCanvas *canvas;
	GooCanvasItem *text;
	GooCanvasItem *table;
	GooCanvasItem *subtable;
	GooCanvasItem *image;
	GooCanvasItem *bar;
	cairo_pattern_t *pattern;
	cairo_pattern_t *pattern2;
	gint i;
	gint j;

	gtk_init(&argc, &argv);

	pattern = cairo_pattern_create_linear(0, 0, 0, 45.0);
	cairo_pattern_add_color_stop_rgb(pattern, 0, .823, .815, .811);
	cairo_pattern_add_color_stop_rgb(pattern, 1, .662, .647, .635);

	pattern2 = cairo_pattern_create_linear(0, 0, 0, 45.0);
	cairo_pattern_add_color_stop_rgb(pattern2, 0, .56, .67, .803);
	cairo_pattern_add_color_stop_rgb(pattern2, 1, .305, .462, .658);

	window = g_object_new(GTK_TYPE_WINDOW,
	                      "default-width", 640,
	                      "default-height", 480,
	                      NULL);

	vbox = g_object_new(GTK_TYPE_VBOX,
	                    "visible", TRUE,
	                    NULL);
	gtk_container_add(GTK_CONTAINER(window), vbox);

	paned = g_object_new(GTK_TYPE_VPANED,
	                     "visible", TRUE,
	                     NULL);
	gtk_container_add(GTK_CONTAINER(vbox), paned);

	layout = g_object_new(GTK_TYPE_TABLE,
	                      "n-columns", 3,
	                      "n-rows", 4,
	                      "visible", TRUE,
	                      NULL);
	gtk_container_add_with_properties(GTK_CONTAINER(paned), layout,
	                                  "resize", TRUE,
	                                  NULL);

	h = g_object_new(PPG_TYPE_HEADER,
	                 "visible", TRUE,
	                 "width-request", 200,
	                 NULL);
	gtk_container_add_with_properties(GTK_CONTAINER(layout), h,
	                                  "top-attach", 0,
	                                  "bottom-attach", 1,
	                                  "left-attach", 0,
	                                  "right-attach", 1,
	                                  "x-options", GTK_FILL,
	                                  "y-options", GTK_FILL,
	                                  NULL);

	ruler = g_object_new(PPG_TYPE_RULER,
	                     "visible", TRUE,
	                     "lower", 0.0,
	                     "upper", 60.0,
	                     NULL);
	gtk_container_add_with_properties(GTK_CONTAINER(layout), ruler,
	                                  "top-attach", 0,
	                                  "bottom-attach", 1,
	                                  "left-attach", 1,
	                                  "right-attach", 2,
	                                  "y-options", GTK_FILL,
	                                  NULL);
	g_signal_connect(ruler, "size-allocate",
	                 G_CALLBACK(ruler_size_allocate), NULL);

	scroller = g_object_new(GTK_TYPE_SCROLLED_WINDOW,
	                        "hscrollbar-policy", GTK_POLICY_ALWAYS,
	                        "shadow-type", GTK_SHADOW_IN,
	                        "visible", TRUE,
	                        "vscrollbar-policy", GTK_POLICY_ALWAYS,
	                        NULL);
	gtk_container_add_with_properties(GTK_CONTAINER(layout), scroller,
	                                  "top-attach", 1,
	                                  "bottom-attach", 2,
	                                  "left-attach", 0,
	                                  "right-attach", 2,
	                                  NULL);

	vadj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(scroller));

	canvas = g_object_new(GOO_TYPE_CANVAS,
	                      "automatic-bounds", TRUE,
	                      "background-color", "#eeeeec",
	                      "visible", TRUE,
	                      NULL);
	gtk_container_add(GTK_CONTAINER(scroller), GTK_WIDGET(canvas));

	l = g_object_new(GTK_TYPE_LABEL,
	                 "label", "XXX",
	                 "visible", TRUE,
	                 NULL);
	gtk_container_add_with_properties(GTK_CONTAINER(paned), l,
	                                  "shrink", FALSE,
	                                  "resize", FALSE,
	                                  NULL);

	table = g_object_new(GOO_TYPE_CANVAS_TABLE,
	                     "parent", goo_canvas_get_root_item(canvas),
	                     "row-spacing", 0.0,
	                     NULL);
	g_signal_connect(canvas, "size-allocate",
	                 G_CALLBACK(size_allocate),
	                 table);
	g_signal_connect(table, "notify::height",
	                 G_CALLBACK(notify_height), NULL);

	for (i = 0; i < 10; i++) {
		image = g_object_new(GOO_TYPE_CANVAS_IMAGE,
							 "height", 45.0 * 2 + 2,
							 "pattern", i == 5 ? pattern2 : pattern,
							 "parent", table,
							 "scale-to-fit", TRUE,
							 "width", 200.0,
							 NULL);
		goo_canvas_item_set_child_properties(table, image,
											 "row", i,
											 "column", 0,
											 "x-expand", FALSE,
											 "x-fill", TRUE,
											 "y-expand", TRUE,
											 "y-fill", TRUE,
											 NULL);

		text = g_object_new(GOO_TYPE_CANVAS_TEXT,
							"parent", table,
							"antialias", CAIRO_ANTIALIAS_SUBPIXEL,
							"text", "Memory",
							"font", "Sans 10",
							"fill-color", i == 5 ? "#ffffff" : "#000000",
							NULL);
		goo_canvas_item_set_child_properties(table, text,
											 "row", i,
											 "column", 0,
											 "x-align", 0.0,
											 "y-align", 0.0,
											 "left-padding", 15.0,
											 "top-padding", 15.0,
											 NULL);

		bar = g_object_new(GOO_TYPE_CANVAS_RECT,
						   "parent", table,
						   "width", 1.0,
						   "height", 45.0 * 2 + 2,
						   "line-width", 0.0,
						   "fill-color", i == 5 ? "#4e76a8" : "#a9a6a2",
						   NULL);
		goo_canvas_item_set_child_properties(table, bar,
											 "row", i,
											 "column", 1,
											 "y-expand", TRUE,
											 "y-fill", TRUE,
											 "y-align", 0.5,
											 "x-expand", FALSE,
											 "x-fill", FALSE,
											 "x-align", 0.0,
											 NULL);

		subtable = g_object_new(GOO_TYPE_CANVAS_TABLE,
		                        "parent", table,
		                        //"row-spacing", 1.0,
		                        NULL);
		goo_canvas_item_set_child_properties(table, subtable,
											 "column", 2,
											 "row", i,
											 "x-expand", TRUE,
											 "x-fill", TRUE,
											 "y-expand", TRUE,
											 "y-fill", TRUE,
											 NULL);

		for (j = 0; j < 2; j++) {
			GooCanvasItem *row;

			row = g_object_new(GOO_TYPE_CANVAS_RECT,
			                   "parent", subtable,
			                   "fill-color", i == 5 ? "#90accf" : "#d4d2d1",
			                   "line-width", 0.0,
			                   "width", 400.0,
			                   "height", 45.0,
			                   NULL);
			goo_canvas_item_set_child_properties(subtable, row,
			                                     "column", 0,
			                                     "row", j,
			                                     "x-align", 0.0,
			                                     "x-expand", TRUE,
			                                     "x-fill", TRUE,
			                                     "y-expand", TRUE,
			                                     "y-fill", TRUE,
		                                         "bottom-padding", 1.0,
			                                     NULL);
			rows = g_list_prepend(rows, row);
		}
	}

	g_timeout_add(1000, animate, paned);

	gtk_window_present(GTK_WINDOW(window));
	g_signal_connect(window, "delete-event", gtk_main_quit, NULL);
	gtk_main();

	return 0;
}
