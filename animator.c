#include <gobject/gvaluecollector.h>
#include <string.h>

#include "animator.h"

PpgAnimation*
g_object_animatev (gpointer          object,
                   PpgAnimationMode  mode,
                   guint             duration_msec,
                   guint             frame_rate,
                   const gchar      *first_property,
                   va_list           args)
{
	PpgAnimation *animation;
	GObjectClass *klass;
	GObjectClass *pklass;
	const gchar *name;
	GParamSpec *pspec;
	GtkWidget *parent;
	GValue value = { 0 };
	gchar *error = NULL;
	GType type;
	GType ptype;

	g_return_val_if_fail(first_property != NULL, NULL);
	g_return_val_if_fail(mode < PPG_ANIMATION_LAST, NULL);

	name = first_property;
	type = G_TYPE_FROM_INSTANCE(object);
	klass = G_OBJECT_GET_CLASS(object);
	animation = g_object_new(PPG_TYPE_ANIMATION,
	                         "duration", duration_msec,
	                         "frame-rate", frame_rate ? frame_rate : 60,
	                         "mode", mode,
	                         "target", object,
	                         NULL);

	do {
		/*
		 * First check for the property on the object. If that does not exist
		 * then check if the object has a parent and look at its child
		 * properties (if its a GtkWidget).
		 */
		if (!(pspec = g_object_class_find_property(klass, name))) {
			if (!g_type_is_a(type, GTK_TYPE_WIDGET)) {
				g_critical("Failed to find property %s in %s",
				           name, g_type_name(type));
				goto failure;
			}
			if (!(parent = gtk_widget_get_parent(object))) {
				g_critical("Failed to find property %s in %s",
				           name, g_type_name(type));
				goto failure;
			}
			pklass = G_OBJECT_GET_CLASS(parent);
			ptype = G_TYPE_FROM_INSTANCE(parent);
			if (!(pspec = gtk_container_class_find_child_property(pklass, name))) {
				g_critical("Failed to find property %s in %s or parent %s",
				           name, g_type_name(type), g_type_name(ptype));
				goto failure;
			}
		}

		g_value_init(&value, pspec->value_type);
		G_VALUE_COLLECT(&value, args, 0, &error);
		if (error != NULL) {
			g_critical("Failed to retrieve va_list value: %s", error);
			g_free(error);
			goto failure;
		}

		ppg_animation_add_property(animation, pspec, &value);
		g_value_unset(&value);
	} while ((name = va_arg(args, const gchar *)));

	ppg_animation_start(animation);

	return animation;

failure:
	g_object_ref_sink(animation);
	g_object_unref(animation);
	return NULL;
}

PpgAnimation*
g_object_animate (gpointer          object,
                  PpgAnimationMode  mode,
                  guint             duration_msec,
                  const gchar      *first_property,
                  ...)
{
	PpgAnimation *animation;
	va_list args;

	va_start(args, first_property);
	animation = g_object_animatev(object, mode, duration_msec, 0,
	                              first_property, args);
	va_end(args);
	return animation;
}

PpgAnimation*
g_object_animate_full (gpointer          object,
                       PpgAnimationMode  mode,
                       guint             duration_msec,
                       guint             frame_rate,
                       GDestroyNotify    notify,
                       gpointer          notify_data,
                       const gchar      *first_property,
                       ...)
{
	PpgAnimation *animation;
	va_list args;

	va_start(args, first_property);
	animation = g_object_animatev(object, mode, duration_msec,
	                              frame_rate, first_property, args);
	va_end(args);
	g_object_weak_ref(G_OBJECT(animation), (GWeakNotify)notify, notify_data);
	return animation;
}
