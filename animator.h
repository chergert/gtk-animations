#ifndef ANIMATOR_H
#define ANIMATOR_H

#include <gtk/gtk.h>

#include "ppg-animation.h"

G_BEGIN_DECLS

PpgAnimation* g_object_animate (gpointer object,
                                PpgAnimationMode mode,
                                guint duration_msec,
                                const gchar *first_property,
                                ...) G_GNUC_NULL_TERMINATED;
PpgAnimation* g_object_animate_full (gpointer object,
                                     PpgAnimationMode mode,
                                     guint duration_msec,
                                     guint frame_rate,
                                     GDestroyNotify notify,
                                     gpointer notify_data,
                                     const gchar *first_property,
                                     ...) G_GNUC_NULL_TERMINATED;

G_END_DECLS

#endif /* ANIMATOR_H */
